django>=2.2
django-appconf>=0.6.0

[test]
pytest
pytest-cov
pytest-django
selenium
